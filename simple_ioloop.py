import sys
import types
import queue
import threading
from collections import deque


class BlockingOperation:
    def __init__(self, blocking_func, args=None, kwargs=None):
        self.caller_id = None
        # something that will execute in a blocking way
        self.blocking_func = blocking_func
        self.args = args or []
        self.kwargs = kwargs or {}
        # result of I/O operation
        self.return_value = None

    def run_blocking(self):
        self.return_value = self.blocking_func(*self.args, **self.kwargs)


class IOThread(threading.Thread):
    def __init__(self, buffer_size):
        super().__init__()
        self.in_buffer = queue.Queue(maxsize=buffer_size)
        self.out_buffer = queue.Queue(maxsize=buffer_size)

    def next_in(self, operation):
        self.in_buffer.put_nowait(operation)

    def next_out(self):
        return self.out_buffer.get_nowait()

    def finish(self):
        self.in_buffer.put(None)

    def run(self):
        while True:
            operation = self.in_buffer.get()
            if operation is None:
                break
            operation.run_blocking()
            self.in_buffer.task_done()
            self.out_buffer.put(operation)


class IOLoop:
    def __init__(self):
        self.uninitalized_routines = []
        self.routines_queue = deque([])
        self.io_buffer = {}

    def push(self, func, args=None, kwargs=None):
        self.uninitalized_routines.append((func, args or [], kwargs or {},))

    def run(self, run_sync=False):
        # start io thread
        self.io_thread = IOThread(buffer_size=10)
        self.io_thread.start()

        # run all routines sequentially
        for routine, args, kwargs in self.uninitalized_routines:
            generator = routine(*args, **kwargs)
            if isinstance(generator, types.GeneratorType):
                try:
                    operation = next(generator)
                    if isinstance(operation, BlockingOperation):
                        # send I/O operation to be executed with ID
                        current_routine_id = id(generator)
                        operation.caller_id = current_routine_id

                        if run_sync:
                            operation.run_blocking()
                            self.io_buffer[current_routine_id] = operation
                        else:
                            self.io_thread.next_in(operation)

                        self.routines_queue.append(generator)
                except StopIteration:
                    pass

        # run the actual I/O loop
        while self.routines_queue:
            # get next routine to be run
            current_routine = self.routines_queue.popleft()

            # check if result of I/O is in the input buffer for the current_routine
            operation = None
            current_routine_id = id(current_routine)
            try:
                if current_routine_id in self.io_buffer:
                    io_result = self.io_buffer[current_routine_id]
                    operation = current_routine.send(io_result.return_value) #, RoutineEnded) # get next operation

                    if isinstance(operation, BlockingOperation):
                        # send I/O operation to be executed with ID
                        operation.caller_id = current_routine_id

                        if run_sync:
                            operation.run_blocking()
                            self.io_buffer[current_routine_id] = operation
                        else:
                            self.io_thread.next_in(operation)
                        # add routine back to the queue to receive the operation result later
                        self.routines_queue.append(current_routine)
                else:
                    self.routines_queue.append(current_routine)
            except StopIteration:
                # nothing to do with this routine, just continue
                pass

            # check I/O results
            try:
                operation = self.io_thread.next_out()
                self.io_buffer[operation.caller_id] = operation
            except queue.Empty:
                pass

        # stop IO thread
        print("loop finished")
        self.io_thread.finish()
        self.io_thread.join()


def write_file():
    file_obj = yield BlockingOperation(open, ('myfile.txt', 'w',))
    yield BlockingOperation(file_obj.write, ("Cool!\n",))
    yield BlockingOperation(file_obj.close)


def print_messages(msgs):
    for m in msgs:
        yield BlockingOperation(sys.stdout.write, ("{}\n".format(m),))


def main():
    io_loop = IOLoop()
    io_loop.push(print_messages, ([1, 3, 5],))
    io_loop.push(write_file)
    io_loop.push(print_messages, ([2, 4],))
    io_loop.run()


if __name__ == '__main__':
    main()
